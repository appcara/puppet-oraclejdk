Facter.add("oraclejdk_installed") do
  confine :kernel => "Linux"
  setcode do
    `java -version 2>&1 | head -n2 | tail -n1`.start_with?('Java(TM)')
  end
end
