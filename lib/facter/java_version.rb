Facter.add("java_version") do
  confine :kernel => "Linux"
  setcode do
    `java -version 2>&1 | head -n1`.split("\"")[1] || ""
  end
end
