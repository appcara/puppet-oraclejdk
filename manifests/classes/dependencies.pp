# Definition installing dependencies
define oraclejdk::package {

  if ! defined(Package[$name]) {
    package { $name:
      ensure => installed
    }
  }

}

# Class declaration for os specific dependencies
class oraclejdk::dependencies() {
  include oraclejdk::params

  case $::operatingsystem {
    Ubuntu: { require oraclejdk::dependencies::ubuntu }
    Amazon,CentOS,RedHat: { require oraclejdk::dependencies::centos }
  }
}
