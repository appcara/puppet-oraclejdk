# Class: oraclejdk
#
# This class installs Oracle JDK available
#
# Parameters:
#   - major version (eg. 6 for Oracle JDK 6, 7 for Oracle JDK 7)
#
# Actions:
#   - Install Oracle JDK
#
# Requires:
#
# Sample Usage:
#
class oraclejdk(){
  include oraclejdk::params

  if ($oraclejdk::params::server) and ($oraclejdk::params::server['recipes']['oraclejdk']['general'][0]) {

    class {'oraclejdk::dependencies': }
    class {'oraclejdk::install': }

    anchor { 'oraclejdk::begin': } ->
      Class['oraclejdk::dependencies'] ->
      Class['oraclejdk::install'] ->
    anchor { 'oraclejdk::end': }

    file { 'oraclejdk-fact':
      ensure => present,
      name   => '/etc/facter/facts.d/appcara_oraclejdk_ready.sh',
      source => 'puppet:///modules/oraclejdk/external_fact.sh',
      owner  => root,
      group  => root,
      mode   => '0755'
    }

  }
}
