class oraclejdk::dependencies::centos {

  if $::operatingsystem == 'CentOS' and $::operatingsystemrelease > 5.8 {
    oraclejdk::package { 'libcurl-devel': }
  } elsif $::operatingsystem == 'RedHat' or $::operatingsystem == 'Amazon' {
    oraclejdk::package { 'libcurl-devel': }
  } else {
    oraclejdk::package { 'curl-devel': }
  }

}
