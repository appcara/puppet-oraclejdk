# Install Oracle JDK
class oraclejdk::install(){
  include oraclejdk::params

  # Install Oracle JDK
  # ==========================================================================

  # check oracle java exists, if it is and not the same version, install it and
  # the installation process will do once only

  $java_home = $oraclejdk::params::full_install_path

  if ( !str2bool("${::oraclejdk_installed}") or $::java_version != $oraclejdk::params::release ) {
    file { 'create-install-path':
      path   => $oraclejdk::params::install_path,
      ensure => directory,
      mode   => '0755',
      owner  => 'root',
      group  => 'root',
    }

    appcara_downloader { "jdk-${oraclejdk::params::version}-${oraclejdk::params::ostype}-x64${oraclejdk::params::file_ext}":
      ensure      => present,
      url         => $oraclejdk::params::download_url,
      download_to => '/usr/local/src',
      require     => File['create-install-path'],
    }

    exec { 'install-oraclejdk':
      command => "tar zxf jdk-${oraclejdk::params::version}-${oraclejdk::params::ostype}-x64${oraclejdk::params::file_ext} -C ${oraclejdk::params::install_path}",
      cwd     => '/usr/local/src',
      creates => $java_home,
      timeout => 0,
      require => Appcara_downloader["jdk-${oraclejdk::params::version}-${oraclejdk::params::ostype}-x64${oraclejdk::params::file_ext}"],
    }

    oraclejdk::update_alternatives { $oraclejdk::params::java_binary :
      path      => "${java_home}/bin",
      subscribe => Exec['install-oraclejdk'],
    }

    if $::operatingsystem == 'Amazon' {
      exec { 'aws-java-home':
        command     => "ln -sfn ${java_home} ${oraclejdk::params::install_path}/java",
        before      => File['java-env-var'],
        subscribe   => Exec['install-oraclejdk'],
        refreshonly => true,
      }
    }

    file { 'java-env-var':
      ensure  => file,
      path    => '/etc/profile.d/jdk_env.sh',
      mode    => '0644',
      content => template('oraclejdk/jdk_env.sh.erb'),
    }
  }
}
