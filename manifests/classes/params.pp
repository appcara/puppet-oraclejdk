# Params for oraclejdk module
class oraclejdk::params {

  $repo_url = "http://APPCARAREPOURL/jdk"
  $file_ext = $::kernel ? {
    'Windows' => '.exe',
    'Linux'   => '.tar.gz',
  }
  $ostype = downcase($::kernel)

  $server = $appcara::params::server

  $install_path = "/usr/lib/jvm"

  if $server {
    $version = $server["recipes"]["oraclejdk"]["general"][0]["version"]
    $java_release = split($version,'u')
    $java_major = $java_release[0]
    $java_build = $java_release[1]

    $download_url = $java_major ? {
      '6' => "${repo_url}/6/jdk-${version}-${ostype}-x64${file_ext}",
      '7' => "${repo_url}/7/jdk-${version}-${ostype}-x64${file_ext}",
      '8' => "${repo_url}/8/jdk-${version}-${ostype}-x64${file_ext}",
    }

    $foldername = $java_major ? {
      '6' => "jdk1.6.0_${java_build}",
      '7' => "jdk1.7.0_${java_build}",
      '8' => "jdk1.8.0_${java_build}",
    }

    $release = $java_major ? {
      '6' => "1.6.0_${java_build}",
      '7' => "1.7.0_${java_build}",
      '8' => "1.8.0_${java_build}",
    }

    $full_install_path = "${install_path}/${foldername}"

    $java_binary = ['java','javac','jar','javaws']
  }

}
