# Update Java alternatives
define oraclejdk::update_alternatives(
    $link = "/usr/bin/${name}",
    $path = undef,
    $priority = '1000'
  )
{
  include oraclejdk::params

  exec {"update-alternatives-${name}":
    command     => "update-alternatives --install ${link} ${name} ${path}/${name} ${priority} && update-alternatives --set ${name} ${path}/${name}",
    path        => $::path,
    refreshonly => true,
  }

}
