#! /bin/bash

java -version 2>&1 | head -n2 | tail -n1 | grep ^Java\(TM\) && echo "appcara_oraclejdk_ready=true" || "appcara_oraclejdk_ready=false"
