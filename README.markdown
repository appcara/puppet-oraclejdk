Puppet Module for Java JDK
==============================================

This module will install Oracle JDK 1.8/1.7/1.6 system widely (any users can use it), the package is uploaded to internal repo.

To enable the factor addon, two steps are needed

Import java module in modules.pp

    import "oraclejdk"

Enable plugin synchronization for custom types.  In puppet.conf

    [main]
        pluginsync = true


## Usage

Install Java JDK with:

    class { "oraclejdk":
      version => '7u1',   # or '6u33'
    }

This will install JDK into `/usr/lib/jvm` system widely.

Versions supported: 
 - 8u40
 - 7u75
 - 6u45

Tested in:

    ubuntu 12.04/14.04 64 bits

